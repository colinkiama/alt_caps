use rand::Rng;
use std::env;
use std::io::Write;
use std::io::{self, Read};
use unicode_segmentation::UnicodeSegmentation;

fn alternate_caps(original_string: &String) {
    const LOWER_RANGE: i8 = 0;
    const UPPER_RANGE: i8 = 100;
    const EXPECTED_BEHAVIOUR_PERCENTAGE: i8 = 70;
    let mut should_be_lower = true;
    let mut rng = rand::thread_rng();

    let print_alt_case_char = |grapheme: &&str| -> () {
        let rng_value = rng.gen_range(LOWER_RANGE, UPPER_RANGE);
        if should_be_lower {
            if rng_value < EXPECTED_BEHAVIOUR_PERCENTAGE {
                print!("{}", grapheme.to_lowercase());
            } else {
                print!("{}", grapheme);
            }
        } else {
            if rng_value < EXPECTED_BEHAVIOUR_PERCENTAGE {
                print!("{}", grapheme.to_uppercase());
            } else {
                print!("{}", grapheme);
            }
        }
        should_be_lower = !should_be_lower;
        if let Err(err) = io::stdout().flush() {
            eprintln!("Error while printing characters: {}", err);
        }
        ()
    };

    let original_string_slice = original_string.as_str();

    println!("Input String: {}", original_string_slice);

    let graphemes =
        UnicodeSegmentation::graphemes(original_string_slice, true).collect::<Vec<&str>>();

    graphemes.iter().for_each(print_alt_case_char);
    println!("");
}

fn main() {
    let args: Vec<String> = env::args().collect();
    match args.len() {
        // User is free to type until they press "prev_grapheme.len() + current_grapheme.len()CTRL + D"
        // to siginify the end of a file EOF.
        1 => {
            println!("Enter the text that you want to convert (Press CTRL + D when done):");
            let mut buffer = String::new();
            let read_result = io::stdin().read_to_string(&mut buffer);
            println!("Now converting text to alternating caps...");
            match read_result {
                Ok(_) => {
                    alternate_caps(&buffer);
                }
                Err(err) => {
                    eprintln!("Something went wrong. Details: {}", err);
                    std::process::exit(1);
                }
            }
        }
        // Get origianl string from arg then process it
        2 => {
            let original_string = &args[1];
            alternate_caps(original_string);
        }
        _ => {
            eprintln!("Too many arguments have been entered. You only need 1");
            std::process::exit(1);
        }
    }
}
