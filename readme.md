# alt_caps

This program converts any text into alternating caps text (wHiCh lOoks liKe tHiS).

## How to use:
```Shell
# Allows you to enter text that will be converted when you're done.
alt-caps
```

```Shell
# Converts any text that you enter
alt_caps {MY_TEXT}
```
```Shell
# Converts result of previous program's exectution (pipes)
{PREVIOUS_COMMAND} | alt_caps
```

